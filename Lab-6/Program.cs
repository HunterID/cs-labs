﻿using System;
using Lab06Lib;

namespace Lab_6
{
    class Program
    {
        static void Main(string[] args)
        {
            Region[] region;
            Console.WriteLine("Enter the number of regions: ");
            int cntRegions = int.Parse(Console.ReadLine());
            region = new Region[cntRegions];

            for (int i = 0; i < cntRegions; ++i)
            {
                Console.WriteLine("Enter region name: ");
                string sName = Console.ReadLine();
                Console.WriteLine("Enter the country in which the region is located: ");
                string sCountry = Console.ReadLine();
                Console.WriteLine("Enter the time zone: ");
                string sTimeZone = Console.ReadLine();
                Console.WriteLine("Enter the national language of the region: ");
                string sNationalLanguage = Console.ReadLine();
                Console.WriteLine("Enter internet domain of the region: ");
                string sInternetDomain = Console.ReadLine();
                Console.WriteLine("Enter population of the region: ");
                string sPopulation = Console.ReadLine();
                Console.WriteLine("Enter the region's annual income: ");
                string sAnnualIncome = Console.ReadLine();
                Console.WriteLine("Enter the year of foundation of the region: ");
                string srAge = Console.ReadLine();
                Console.WriteLine("Enter the area of the region: ");
                string sSquare = Console.ReadLine();

                Region theRegion = new Region();
                theRegion.Name = sName;
                theRegion.Country = sCountry;
                theRegion.TimeZone = sTimeZone;
                theRegion.NationalLanguage = sNationalLanguage;
                theRegion.InternetDomain = sInternetDomain;
                theRegion.Population = int.Parse(sPopulation);
                theRegion.AnnualIncome = int.Parse(sAnnualIncome);
                theRegion.rAge = int.Parse(srAge);
                theRegion.Square = double.Parse(sSquare);

                region[i] = theRegion;
            }

            foreach(Region rg in region)
            {
                Console.WriteLine("---------------------------------------------");
                Console.WriteLine($"Region name: {rg.Name}");
                Console.WriteLine($"Country in which the region is located: {rg.Country}");
                Console.WriteLine($"Time zone: {rg.TimeZone}");
                Console.WriteLine($"The national language of the region: {rg.NationalLanguage}");
                Console.WriteLine($"Internet domain of the region: {rg.InternetDomain}");
                Console.WriteLine($"Population of the region: {rg.Population}");
                Console.WriteLine($"Region's annual income: {rg.AnnualIncome}");
                Console.WriteLine($"Year of foundation of the region: {rg.rAge}");
                Console.WriteLine($"Region age: {rg.RegionAge()}");
                Console.WriteLine($"Area of the region: {rg.Square}");
                Console.WriteLine($"Average annual income: {rg.AverangeIncome()}");
                Console.WriteLine("---------------------------------------------");
            }

            Console.ReadKey();
        }
    }
}
