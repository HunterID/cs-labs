﻿using System;

namespace Lab_5
{
    class Program
    {
        static void menu()
        {
            Console.WriteLine("[E] Enter region data");
            Console.WriteLine("[D] Display data");
            Console.Write("");
        }
        static void Main(string[] args)
        {
            Region Rg = new Region();

        Mn:
            menu();
            ConsoleKeyInfo pressedKey = Console.ReadKey();
            Console.WriteLine();
            if (pressedKey.Key == ConsoleKey.E)
            {
                Console.WriteLine();
                goto Start;
            }
            else if (pressedKey.Key == ConsoleKey.D)
            {
                Console.WriteLine();
                goto Result;
            }
            else Console.WriteLine("Smth went wrong...");

        Start:
            Console.WriteLine("Enter region name: ");
            Rg.Name = Console.ReadLine();
            Console.WriteLine("Enter the country in which the region is located: ");
            Rg.Country = Console.ReadLine();
            Console.WriteLine("Enter the time zone: ");
            Rg.TimeZone = Console.ReadLine();
            Console.WriteLine("Enter the national language of the region: ");
            Rg.NationalLanguage = Console.ReadLine();
            Console.WriteLine("Enter internet domain of the region: ");
            Rg.InternetDomain = Console.ReadLine();
            Console.WriteLine("Enter population of the region: ");
            Rg.Population = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter the region's annual income: ");
            Rg.AnnualIncome = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter the year of foundation of the region: ");
            Rg.rAge = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter the area of the region: ");
            Rg.Square = double.Parse(Console.ReadLine());
            goto Mn;

        Result:
            Console.WriteLine("---------------------------------------------");
            Console.WriteLine($"Region name: {Rg.Name}");
            Console.WriteLine($"Country in which the region is located: {Rg.Country}");
            Console.WriteLine($"Time zone: {Rg.TimeZone}");
            Console.WriteLine($"The national language of the region: {Rg.NationalLanguage}");
            Console.WriteLine($"Internet domain of the region: {Rg.InternetDomain}");
            Console.WriteLine($"Population of the region: {Rg.Population}");
            Console.WriteLine($"Region's annual income: {Rg.AnnualIncome}");
            Console.WriteLine($"Year of foundation of the region: {Rg.rAge}");
            Console.WriteLine($"Region age: {Rg.RegionAge()}");
            Console.WriteLine($"Area of the region: {Rg.Square}");
            Console.WriteLine($"Average annual income: {Rg.AverangeIncome()}");
            Console.WriteLine("---------------------------------------------");

            goto Mn;
        }
    }
}
