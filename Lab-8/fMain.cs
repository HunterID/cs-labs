﻿using System;
using Lab08Lib;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab_8
{
    public partial class fMain : Form
    {
        public fMain()
        {
            InitializeComponent();
        }

        private void FMain_Load(object sender, EventArgs e)
        {

        }

        private void BtnAddRegion_Click(object sender, EventArgs e)
        {
            _Region region = new _Region();
            fRegion fr = new fRegion(region);

            if(fr.ShowDialog() == DialogResult.OK)
            {
                tbRegionsInfo.Text += string.Format($"{region.Name}, {region.Country}, Національна мова: {region.NationalLanguage}, " +
                    $"Інтернет Домен {region.InternetDomain}, Населення: {region.Population}, Річний дохід: {region.AnnualIncome}, " +
                    $"Площа: {region.Square}, " + "[ {0} | {1}], Середній дохід на мешканця: {2:0.00}\r\n", region.HasPort ? "Є порт" : "Немає порта",
                    region.HasAirport ? "Є аеропорт" : "Немає аеропорта", region.AverangeIncome());
            }
        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Припинити роботу застосунку?", "Припинити роботу", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                Application.Exit();
        }
    }
}
