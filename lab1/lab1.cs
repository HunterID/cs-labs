﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {   Console.WriteLine("====== 15 Variant =======");
            Console.Write("Введiть початкове значення Xmin: ");
            string sxMin = Console.ReadLine();
            double xMin = Double.Parse(sxMin);
            Console.Write("Введiть кiнцеве значення Xmax: ");
            string sxMax = Console.ReadLine();
            double xMax = double.Parse(sxMax);
            Console.Write("Введiть прирiст dX: ");
            string sdx = Console.ReadLine();
            double dx = double.Parse(sdx);
            double x = xMin;
            double y;
            double svz = 1 / 4 + x * 3;
            double fullsum = x;

            while (x <= xMax)
            {
                y = Math.Sqrt(Math.Pow(Math.Exp(x*3/x),2)) * svz;
                Console.WriteLine("x = {0}\t\t y = {1}", x, y);
                x += dx;
                fullsum += x;
            }
            if (Math.Abs(x - xMax - dx) > 0.0001)
            {
                y = Math.Pow(xMax, 2);
                Console.WriteLine("x = {0}\t\t y = {1}", xMax, y);
            }
            Console.WriteLine("Сумма промежуточных результатов :" + fullsum);
            Console.ReadKey();
        }
    }
}