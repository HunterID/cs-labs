﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("------------ Variant 15 -------------");
            Console.Write("Введіть початкове значення для X1Min:");
            string sx1Min = Console.ReadLine();
            double x1Min = Double.Parse(sx1Min);

            Console.Write("Введіть початкове значення для X1Max:");
            string sx1Max = Console.ReadLine();
            double x1Max = Double.Parse(sx1Max);

            Console.Write("Введіть приріст dx1:");
            string sdx1 = Console.ReadLine();
            double dx1 = Double.Parse(sdx1);

            Console.Write("Введіть початкове значення для X2Min:");
            string sx2Min = Console.ReadLine();
            double x2Min = Double.Parse(sx2Min);

            Console.Write("Введіть початкове значення для X2Max:");
            string sx2Max = Console.ReadLine();
            double x2Max = Double.Parse(sx2Max);

            Console.Write("Введіть приріст dx2:");
            string sdx2 = Console.ReadLine();
            double dx2 = Double.Parse(sdx2);

            double y;

            double x1 = x1Min;
            double x2;

            double Fullsum = 0;

            while (x1 < x1Max)
            {
                x2 = x2Min;
                while (x2 <= x2Max)
                {
                    y = Math.Sqrt(56 * x1 + x1 + x2 + Math.Sin(x1 * x2) / 5 - Math.Cos(Math.Pow(x2, 2)));
                    Console.WriteLine("x1 = {0:#.###}\tx2 = {1:#.###}\t\ty = {2:#.###}", x1, x2, y);
                    x2 += dx2;
                    Fullsum += y;
                }
                x1 += dx1;
            }
            Console.Write("Сума всіх проміжних значень:" + Fullsum);
            Console.ReadKey();
        }
    }
}